<%--
  Created by IntelliJ IDEA.
  User: Hong Hai Nguyen
  Date: 7/13/2017
  Time: 8:51 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="container-fluid text-center">
    <span class="slogan">If you don't practice, you don't deserve to win!</span>
</footer>