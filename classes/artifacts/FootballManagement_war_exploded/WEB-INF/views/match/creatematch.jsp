<%--
  Created by IntelliJ IDEA.
  User: Hong Hai Nguyen
  Date: 7/14/2017
  Time: 11:50 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>

<head>
    <title>${title}</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /*tr:first-child{*/
        /*font-weight: bold;*/
        /*background-color: lightcoral;*/
        /*}*/
        .error {
            color: #4aff6f;
        }
        .width-100
        {
            width: 100% !important;
        }
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 450px}

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            background-color: #f1f1f1;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }

        body{
            margin:0;
        }

        /*table{*/
        /*border-collapse:collapse;*/
        /*width:100%;*/
        /*text-align:center;*/

        /*}*/

        .slogan{
            font-size: 30px;
            font-family: Arial;
            text-align: center;
        }

        h2{
            font-size: 32px;
            font-family: Georgia;
        }

        table-header{
            font-weight:bold;
            height:50px;
        }
        tr{
            height:30px;
        }
        /*td{*/
        /*padding:10px;*/
        /*border:1px solid #ddd;*/
        /*border-color: plum;*/

        /*}*/
        .content {
            height: 100%;
        }

        label {
            width: 250px;
            text-align: left;
        }

        form {
            width: 250px;
        }


        /*.formhead{*/
        /*background-color: antiquewhite;*/
        /*}*/

        /*body{*/
        /*background-image: url("");*/
        /*}*/

    </style>

</head>

<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">GEM Ladies Football</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
                <li><a href="/players">Players</a></li>
                <li><a href="/teams">Football Clubs</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Matches
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="/matches">All Matches</a></li>
                        <li><a href="/matches/occurred">Occurred Matches</a></li>
                        <li><a href="/matches/comingup">Coming Up Matches</a></li>
                    </ul>
                </li>
                <li><a href="/guessscore/">Guess Score</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <p><a href="#">Here will be a link of football news in the world</a></p>
            <hr>
            <p><a href="#">Here will be Viet Nam football news link</a></p>
        </div>
        <div class="col-sm-8 text-left">
            <div class="formhead">
                <h2>Create A Match</h2>
                <hr>
            </div>

            <div class="table">
                <form:form method="POST" modelAttribute="matches">
                    <%--<form:input type="hidden" path="id" id="id"/>--%>
                    <table >
                        <tr>
                            <td><label>Home Team: </label> </td>
                            <%--<td><form:input path="hometeam.name" id="hometeam"/></td>--%>
                            <%--<td><form:errors path="hometeam.name" cssClass="error"/></td>--%>
                            <td><form:select path="hometeam.id" class="form-control" id = "hometeam" width = "100%">
                                <form:options items="${teams}" />
                            </form:select></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td><label>Goal's home team: </label> </td>
                            <td><form:input path="home_team_score" id="home_team_score" class="form-control" /></td>
                            <td><form:errors path="home_team_score" cssClass="error"/></td>
                        </tr>

                        <tr>
                            <td><label>Away Team: </label> </td>
                            <%--<td><form:input path="awayteam.name" id="awayteam"/></td>--%>
                            <%--<td><form:errors path="awayteam.name" cssClass="error"/></td>--%>
                            <td><form:select path="awayteam.id" id = "awayteam" class="form-control">
                                <form:options items="${teams}"/>
                            </form:select></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td><label>Goal's home team: </label> </td>
                            <td><form:input path="away_team_score" id="away_team_score" class="form-control"/></td>
                            <td><form:errors path="away_team_score" cssClass="error"/></td>
                        </tr>

                        <%--<tr>--%>
                            <%--<td><label>Date: </label> </td>--%>
                            <%--&lt;%&ndash;<td><fmt:formatDate value="start_date" pattern="dd/mm/yyyy" /> </td>&ndash;%&gt;--%>
                            <%--<td><form:input path="start_date" id="start_date" type="date" pattern="dd/MM/yyyy" class="form-control"/></td>--%>
                            <%--<td><form:errors path="start_date" cssClass="error"/></td>--%>
                        <%--</tr>--%>

                        <tr>
                            <td><label>Time: </label> </td>
                            <td><form:input path="start_time" id="start_time" type="datetime-local" class="form-control"/></td>
                            <td><form:errors path="start_time" cssClass="error"/></td>
                        </tr>

                        <tr></tr>

                        <tr>
                            <td colspan="3">
                                <c:choose>
                                    <c:when test="${edit}">
                                        <input type="submit" value="Update"/>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="submit" value="Register"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>

                        </tr>

                    </table>
                </form:form>
            </div>

        </div>

        <div class="col-sm-2 sidenav">
            <div class="well">
                <p>Here will be something about GEM ladies league</p>
            </div>
            <div class="well">
                <p>Here will be a photo</p>
            </div>
        </div>
    </div>
</div>

<%@include file="../footer.jsp"%>


<br/>
<br/>
Go back to <a href="<c:url value='/matches' />">List of all matches</a>
</body>
</html>

