<%--<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>--%>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%--&lt;%&ndash;--%>
  <%--Created by IntelliJ IDEA.--%>
  <%--User: Hong Hai Nguyen--%>
  <%--Date: 8/8/2017--%>
  <%--Time: 6:27 PM--%>
  <%--To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<html>--%>
<%--<head>--%>
    <%--<title>Log in</title>--%>
    <%--<style>--%>
        <%--/* Bordered form */--%>
        <%--form {--%>
            <%--border: 3px solid #f1f1f1;--%>
        <%--}--%>

        <%--/* Full-width inputs */--%>
        <%--input[type=text], input[type=password] {--%>
            <%--width: 100%;--%>
            <%--padding: 12px 20px;--%>
            <%--margin: 8px 0;--%>
            <%--display: inline-block;--%>
            <%--border: 1px solid #ccc;--%>
            <%--box-sizing: border-box;--%>
        <%--}--%>

        <%--/* Set a style for all buttons */--%>
        <%--button {--%>
            <%--background-color: #4CAF50;--%>
            <%--color: white;--%>
            <%--padding: 14px 20px;--%>
            <%--margin: 8px 0;--%>
            <%--border: none;--%>
            <%--cursor: pointer;--%>
            <%--width: 100%;--%>
        <%--}--%>

        <%--/* Add a hover effect for buttons */--%>
        <%--button:hover {--%>
            <%--opacity: 0.8;--%>
        <%--}--%>

        <%--/* Extra style for the cancel button (red) */--%>
        <%--.cancelbtn {--%>
            <%--width: auto;--%>
            <%--padding: 10px 18px;--%>
            <%--background-color: #f44336;--%>
        <%--}--%>

        <%--/* Center the avatar image inside this container */--%>
        <%--.imgcontainer {--%>
            <%--text-align: center;--%>
            <%--margin: 24px 0 12px 0;--%>
        <%--}--%>

        <%--/* Avatar image */--%>
        <%--img.avatar {--%>
            <%--width: 40%;--%>
            <%--border-radius: 50%;--%>
        <%--}--%>

        <%--/* Add padding to containers */--%>
        <%--.container {--%>
            <%--padding: 16px;--%>
            <%--text-align: center;--%>
        <%--}--%>

        <%--/* The "Forgot password" text */--%>
        <%--span.psw {--%>
            <%--float: right;--%>
            <%--padding-top: 16px;--%>
            <%--text-align: center;--%>
        <%--}--%>

        <%--/* Change styles for span and cancel button on extra small screens */--%>
        <%--@media screen and (max-width: 300px) {--%>
            <%--span.psw {--%>
                <%--display: block;--%>
                <%--float: none;--%>
            <%--}--%>
            <%--.cancelbtn {--%>
                <%--width: 100%;--%>
            <%--}--%>
        <%--}--%>

    <%--</style>--%>
<%--</head>--%>
<%--<body>--%>

    <%--<div class="container">--%>
        <%--<form:form method="POST" modelAttribute="user">--%>
            <%--&lt;%&ndash;<form:input type="hidden" path="id" id="id"/>&ndash;%&gt;--%>
            <%--<table class="register" align="center">--%>
                <%--<tr>--%>
                    <%--<td><label for="username">Username: </label> </td>--%>
                    <%--<td><form:input path="username" id="username"/></td>--%>
                    <%--<td><form:errors path="username" cssClass="error"/></td>--%>
                <%--</tr>--%>

                <%--<tr>--%>
                    <%--<td><label for="password">Password: </label> </td>--%>
                    <%--<td><form:input path="password" id="password"/></td>--%>
                    <%--<td><form:errors path="password" cssClass="error"/></td>--%>
                <%--</tr>--%>

                <%--<tr align="center">--%>
                    <%--<td colspan="3">--%>

                        <%--<input type="submit" value="Submit"/>--%>

                    <%--</td>--%>
                <%--</tr>--%>

            <%--</table>--%>
        <%--</form:form>--%>

    <%--</div>--%>

<%--------------------------%>

<html>
<head>
    <title>Log in</title>
    <style>
        /* Bordered form */
        form {
            border: 3px solid #f1f1f1;
        }

        /* Full-width inputs */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }

        /* Set a style for all buttons */
        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        /* Add a hover effect for buttons */
        button:hover {
            opacity: 0.8;
        }
        h2 {
            text-align: center;
        }

        /* Add padding to containers */
        .container {
            padding: 16px;
        }

        /* The "Forgot password" text */
        span.psw {
            float: right;
            padding-top: 16px;
            text-align: center;
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }
            .cancelbtn {
                width: 100%;
            }
        }

        /*body {*/
            /*background-color: #cccccc;*/
            /*background-repeat: no-repeat;*/
            /*width: 100%;*/
            /*height: 100%;*/
        /*}*/

        /*.top {*/
            /*height: 200px;*/
        /*}*/

        span.login {
            font-family: "Lucida Bright";
            font-size: 70px;
            text-align: center;
        }



    </style>
</head>
<body>
<img src="https://ichef.bbci.co.uk/images/ic/480x270/p05394v7.jpg" style="position: absolute;height: 100%;width: 100%">
<div style="position: relative ; width: 30%;margin: auto;" >
    <div>
        <div style="border: 3px solid lightseagreen; margin-top: 46px; margin-bottom: 117px; height: 74px; width: 36%; text-align: center; margin-left: 32%; background-color: #1abc9c">
            <span><font color="white" font-family = "Arial" font-size = "30px
">Login</font></span>
        </div>
        <form class="center" name='f' action="login" method='POST' modelAttribute = 'user' >
            <%--<div class="imgcontainer">--%>
            <%--<img src="" alt="Avatar" class="avatar">--%>
            <%--</div>--%>

            <div>
                <table align="center">
                    <tr>
                        <td>User:</td>
                        <td><input type='text' name='username' value='' placeholder="Username" class="form-control"></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type='password' name='password' placeholder="Password" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td><input name="submit" type="submit" value="Submit" /></td>
                        <td><input type="checkbox" checked="checked"> Remember me</td>
                    </tr>
                </table>
            </div>

            <div class="container">
                <table align="center">
                    <tr>
                        <td><button type="button" class="cancelbtn">Cancel</button></td>
                        <td><span class="psw">Forgot <a href="#">password?</a></span></td>
                    </tr>
                </table>


            </div>



        </form>
    </div>
</div>
</body>
</html>





<%--</body>--%>
<%--</html>--%>
