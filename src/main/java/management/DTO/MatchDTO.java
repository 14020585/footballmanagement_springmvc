package management.DTO;

import management.entity.Seasons;
import management.entity.Teams;

import java.security.Timestamp;
import java.util.Date;

/**
 * Created by Hong Hai Nguyen on 7/14/2017.
 */
public class MatchDTO {
    private int id;

    private Teams hometeam;

    private Teams awayteam;

    private Date start_time;

    private Seasons seasons;

    private int home_team_score;

    private  int away_team_score;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Teams getHometeam() {
        return hometeam;
    }

    public void setHometeam(Teams hometeam) {
        this.hometeam = hometeam;
    }

    public Teams getAwayteam() {
        return awayteam;
    }

    public void setAwayteam(Teams awayteam) {
        this.awayteam = awayteam;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Seasons getSeasons() {
        return seasons;
    }

    public void setSeasons(Seasons seasons) {
        this.seasons = seasons;
    }

    public int getHome_team_score() {
        return home_team_score;
    }

    public void setHome_team_score(int home_team_score) {
        this.home_team_score = home_team_score;
    }

    public int getAway_team_score() {
        return away_team_score;
    }

    public void setAway_team_score(int away_team_score) {
        this.away_team_score = away_team_score;
    }
}
