package management.DTO;

import java.util.Date;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
public class PlayerDTO extends FatherDTO {
    private String name;

    private  int age;

    private int height;

    private  int weight;

    private Date joined;

    private String position;

    private TeamDTO teams;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public TeamDTO getTeams() {
        return teams;
    }

    public void setTeams(TeamDTO teams) {
        this.teams = teams;
    }
}
