package management.DTO;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
public class TeamDTO extends FatherDTO{

    private String name;

    private PlayerDTO players;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlayerDTO getPlayers() {
        return players;
    }

    public void setPlayers(PlayerDTO players) {
        this.players = players;
    }
}
