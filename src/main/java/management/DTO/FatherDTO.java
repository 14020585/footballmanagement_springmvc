package management.DTO;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
public class FatherDTO {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
