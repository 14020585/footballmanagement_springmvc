package management.entity;

import javax.persistence.*;

@Entity
@Table(name = "guess")
public class GuessScore {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "user_id")
    @ManyToOne
    private User user;

    @JoinColumn(name = "match_id")
    @ManyToOne
    private Matches match;

    @Column(name = "home_team_guess")
    private int home_team_guess;

    @Column(name = "away_team_guess")
    private int away_team_guess;

    @Column(name = "result_guess")
    private boolean result;

    public  GuessScore (){}

    public GuessScore(Matches match) {
        this.match = match;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Matches getMatch() {
        return match;
    }

    public void setMatch(Matches match) {
        this.match = match;
    }

    public int getHome_team_guess() {
        return home_team_guess;
    }

    public void setHome_team_guess(int home_team_guess) {
        this.home_team_guess = home_team_guess;
    }

    public int getAway_team_guess() {
        return away_team_guess;
    }

    public void setAway_team_guess(int away_team_guess) {
        this.away_team_guess = away_team_guess;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
