package management.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Admin on 30/06/2017.
 */
@Entity
@Table(name = "seasons")
public class Seasons {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "league_id")
    private Leagues leagues;

    @ManyToMany
    @JoinTable(
            name = "season_team",
            joinColumns = @JoinColumn(name = "season_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "team_id", referencedColumnName = "id")
    )
    private Set<Teams> teams;

    public Seasons(){}

    public Seasons(int id, String name, Leagues leagues) {
        this.id = id;
        this.name = name;
        this.leagues = leagues;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Leagues getLeagues() {
        return leagues;
    }

    public void setLeagues(Leagues leagues) {
        this.leagues = leagues;
    }
}
