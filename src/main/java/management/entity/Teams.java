package management.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Admin on 30/06/2017.
 */
@Entity
@Table(name = "teams")
public class Teams {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @OneToOne
    @JoinColumn(name = "captain_id")
    private Players players;

    @ManyToMany(mappedBy = "teams")
    private Set<Seasons> seasons;

    public Teams(){}

    public Teams(Integer id, String name, Players players) {
        this.id = id;
        this.name = name;
        this.players = players;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Players getPlayers() {
        return players;
    }

    public void setPlayers(Players players) {
        this.players = players;
    }

    public Set<Seasons> getSeasons() {
        return seasons;
    }

    public void setSeasons(Set<Seasons> seasons) {
        this.seasons = seasons;
    }
}
