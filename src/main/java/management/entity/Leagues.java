package management.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Admin on 30/06/2017.
 */
@Entity
@Table(name = "leagues")
public class Leagues {
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public Leagues(){}

    public Leagues(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
