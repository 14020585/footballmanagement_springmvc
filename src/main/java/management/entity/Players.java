package management.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by Admin on 30/06/2017.
 */
@Entity
@Table(name = "players")
public class Players {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Size(min = 2, max = 50)
    @Column(name = "name")
    private String name;

    @Min(1)
    @Max(100)
    @Column(name = "age")
    private  Integer age;

    @Column(name = "height")
    private Integer height;

    @Column(name = "weight")
    private  Integer weight;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "joined")
    private Date joined;

    @Column(name = "position")
    private String position;

    @ManyToOne
    @JoinColumn(name = "team_id")
    private Teams teams;

    public Players(){}

    public Players(int id, String name, Integer age, Integer height, Integer weight, Date joined, String position, Teams teams) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.joined = joined;
//        this.position = position;
        this.teams = teams;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Date getJoined() {
        return joined;
    }

    public void setJoined(Date joined) {
        this.joined = joined;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }
}
