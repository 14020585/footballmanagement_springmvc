package management.entity;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Admin on 30/06/2017.
 */
@Entity
@Table(name = "matches")
public class Matches {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "home_team_id")
    private Teams hometeam;

    @ManyToOne
    @JoinColumn(name = "away_team_id")
    private Teams awayteam;

//    @NotNull(message = "Date can not be blank")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @Column(name = "start_date")
//    private Date start_date;

    @NotNull (message = "Date can not be blank")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "start_time")
    private Date start_time;

    @ManyToOne
    @JoinColumn(name = "season_id")
    private Seasons seasons;

    @Column(name = "home_team_score")
    private Integer home_team_score;

    @Column(name = "away_team_score")
    private Integer away_team_score;

    public Matches(){}

    public Matches(int id, Teams teams1, Teams teams2, Date start_time, Seasons seasons, int home_team_score, int away_team_score) {
        this.id = id;
        this.hometeam = teams1;
        this.awayteam = teams2;
        this.start_time = start_time;
        this.seasons = seasons;
        this.home_team_score = home_team_score;
        this.away_team_score = away_team_score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Teams getHometeam() {
        return hometeam;
    }

    public void setHometeam(Teams hometeam) {
        this.hometeam = hometeam;
    }

    public Teams getAwayteam() {
        return awayteam;
    }

    public void setAwayteam(Teams awayteam) {
        this.awayteam = awayteam;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Seasons getSeasons() {
        return seasons;
    }

    public void setSeasons(Seasons seasons) {
        this.seasons = seasons;
    }

    public Integer getHome_team_score() {
        return home_team_score;
    }

    public void setHome_team_score(Integer home_team_score) {
        this.home_team_score = home_team_score;
    }

    public Integer getAway_team_score() {
        return away_team_score;
    }

    public void setAway_team_score(Integer away_team_score) {
        this.away_team_score = away_team_score;
    }
}

