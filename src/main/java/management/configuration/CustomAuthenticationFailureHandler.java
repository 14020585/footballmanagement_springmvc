package management.configuration;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sm on 7/12/17.
 */
@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
// Some variables

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        HttpSession session = request.getSession();
        Integer attempt;
        if (session.getAttribute("attempt")==null) {
            session.setAttribute("attempt", new Integer(1));
            response.sendRedirect("/login?error");
        } else {
            attempt = (Integer) session.getAttribute("attempt");
            attempt += 1;
            if (attempt == 3) {
                session.setAttribute("attempt", new Integer(0));
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.append("block login");
                out.close();
            } else
                session.setAttribute("attempt", attempt);
                response.sendRedirect("/login?error");
        }

    }
}