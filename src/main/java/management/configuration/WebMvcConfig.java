package management.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sm on 7/1/17.
 */
@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        // Css resource.
        registry.addResourceHandler("/vendors/**")
                .addResourceLocations("/WEB-INF/resources/vendors/");
        registry.addResourceHandler("/build/**")
                .addResourceLocations("/WEB-INF/resources/build/");
        registry.addResourceHandler("/images/**")
                .addResourceLocations("/WEB-INF/resources/production/images/");
        registry.addResourceHandler("/css/**")
                .addResourceLocations("/WEB-INF/resources/production/css/");
        registry.addResourceHandler("/js/**")
                .addResourceLocations("/WEB-INF/resources/production/js/");
        registry.addResourceHandler("/partials/**")
                .addResourceLocations("/WEB-INF/views/partials/");

    }


    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
        stringConverter.setSupportedMediaTypes(Arrays.asList(new MediaType("text", "plain", UTF8)));
        converters.add(stringConverter);

    }
}
