package management.DAO.impl;

import management.DAO.AbstractDao;
import management.DAO.intf.UserDAO;
import management.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Created by Hong Hai Nguyen on 8/8/2017.
 */
@Repository("userDAO")
public class UserDAOIpml extends AbstractDao<String, User> implements UserDAO {
    @Override
    public User findByUsername(String username) {
//        Criteria criteria = createEntityCriteria();
//        criteria.add(Restrictions.eq("username", username));
//
//        return (User) criteria.uniqueResult();
        Query query = getSession().createQuery("from " + User.class.getName() + " " +
                "where username = :username");
        query.setParameter("username", username);
        return (User) query.uniqueResult();
    }
}
