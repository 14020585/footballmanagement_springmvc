package management.DAO.impl;

import management.DAO.AbstractDao;
import management.DAO.intf.GuessScoreDAO;
import management.entity.GuessScore;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("guessScoreDAO")
public class GuesScoreDAOImpl extends AbstractDao<Long, GuessScore> implements GuessScoreDAO {
    @Override
    public GuessScore findGuessScoreById(Long id) {
        return getByKey(id);
    }

    @Override
    public void saveGuessScore(GuessScore guess) {
        persist(guess);
    }

    @Override
    public void saveRecord(GuessScore guess) {

    }

    @Override
    public boolean checkGuess(int match_id) {
        String hql = "FROM GuessScore WHERE match.id = :_id";
        Query query = getSession().createQuery(hql);
        query.setParameter("_id", match_id);

        if (query.list().size() > 0)
            return true;
        return false;
    }

    @Override
    public List<GuessScore> listAllGuessScoreByUserId(Long id) {
        String hql = "FROM GuessScore WHERE user.id = :_id";
        Query query = getSession().createQuery(hql);
        query.setParameter("_id", id);

        List<GuessScore> result = query.list();
        return result;
    }
}
