package management.DAO.impl;

import management.DAO.AbstractDao;
import management.DAO.intf.PlayerDAO;
import management.entity.Players;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
@Repository("playerDao")
public class PlayerDAOImpl extends AbstractDao<Integer, Players> implements PlayerDAO {
    @Override
    public Players findById(int id) {
        return getByKey(id);
    }

    @Override
    public void savePlayer(Players players) {
        persist(players);
    }

    @Override
    public void deletePlayerById(int id) {
        Query query = getSession().createSQLQuery("delete from players WHERE id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public void setNullTeamId(Players players) {
        players.setTeams(null);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Players> findAllPlayers() {
        Criteria criteria = createEntityCriteria();
        return (List<Players>) criteria.list();
    }

    @Override
    public List<Players> findAllPlayersByTeamId(int teamId) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("teams.id", teamId));

        return (List<Players>) criteria.list();
    }

//    @Override
//    public Players findPlayerByName(String name) {
//        Criteria criteria = createEntityCriteria();
//        criteria.add(Restrictions.eq("name", name));
//
//        return (Players) criteria.uniqueResult();
//    }


//    public static SessionFactory sessionFactory;


//    public static void saveDB(Object object){
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//
//        session.save(object);
//
//        transaction.commit();
//        session.close();
//    }

//    private static void deletePlayer(int id){
//        Session session = sessionFactory.openSession();
//        Transaction transaction = session.beginTransaction();
//
//        Players players = (Players) session.get(Players.class, id);
//        session.delete(players);
//        transaction.commit();
//        session.close();
//    }
}
