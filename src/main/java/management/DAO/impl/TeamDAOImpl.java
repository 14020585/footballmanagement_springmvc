package management.DAO.impl;

import management.DAO.AbstractDao;
import management.DAO.intf.TeamDAO;
import management.entity.Players;
import management.entity.Teams;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/7/2017.
 */
@Repository("teamDao")
public class TeamDAOImpl extends AbstractDao<Integer, Teams> implements TeamDAO {
    @Override
    public Teams findByTeamId(int id) {
        return getByKey(id);
    }

    @Override
    public void saveTeam(Teams teams) {
        persist(teams);
    }

    @Override
    public void deleteTeamById(int id) {
        Query query = getSession().createSQLQuery("DELETE FROM teams WHERE id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Teams> findAllTeams() {
//        String hql = "SELECT T.name FROM teams T";
//        Query query = getSession().createSQLQuery(hql);
//        List result = query.list();
//
//        return result;
        Criteria criteria = createEntityCriteria();

        return (List<Teams>) criteria.list();
    }

    @Override
    public List<Players> listPlayers(int id) {
        String hql = "FROM Players WHERE teams.id = :iddd";
        Query query = getSession().createQuery(hql);
        query.setParameter("iddd", id);
        List result = query.list();

        return result;
    }

    @Override
    public List<Players> abandonPlayers() {
        String hql = "FROM Players WHERE teams.id = null";
        Query query = getSession().createQuery(hql);

        List result = query.list();

        return result;
    }

    @Override
    public void addIntoTeam(int player_id, int team_id) {
        String hql = "UPDATE Players SET teams.id = :team_id_ WHERE id = :player_id_";
        Query query = getSession().createQuery(hql);
        query.setParameter("player_id_", player_id);
        query.setParameter("team_id_", team_id);
        query.executeUpdate();


    }

    @Override
    public List<Teams> findAllTeamsBySeasonName(String name) {
        return null;
    }
}
