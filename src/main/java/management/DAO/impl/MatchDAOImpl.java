package management.DAO.impl;

import management.DAO.AbstractDao;
import management.DAO.intf.MatchDAO;
import management.entity.Matches;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/14/2017.
 */
@Repository("matchDao")
public class MatchDAOImpl extends AbstractDao<Integer, Matches> implements MatchDAO {
    @Override
    public Matches findMatchById(int id) {
        return getByKey(id);
    }

    @Override
    public void saveMatch(Matches matches) {
        persist(matches);
    }

    @Override
    public void deleteMatch(int id) {
        String hql = "DELETE FROM Matches WHERE id = :_id";
        Query query = getSession().createQuery(hql);
        query.setParameter("_id", id);
        query.executeUpdate();

    }

    @Override
    public List<Matches> findAllMatches() {
        Criteria criteria = createEntityCriteria();

        return (List<Matches>) criteria.list();
    }

    @Override
    public List<Matches> findAllMatchComing() {
        String hql = "FROM Matches WHERE start_time >= current_timestamp ";
        Query query = getSession().createQuery(hql);
        List<Matches> result = query.list();

        return result;
    }

    @Override
    public List<Matches> findAllMatchOccured() {
        String hql = "FROM Matches WHERE start_time < current_timestamp ";
        Query query = getSession().createQuery(hql);
        List<Matches> result = query.list();

        return result;
    }
}
