package management.DAO.intf;


import management.entity.UserInfo;

/**
 * Created by sm on 7/2/17.
 */
public interface UserInfoDao {
    UserInfo findUserInfo(String userName);
}
