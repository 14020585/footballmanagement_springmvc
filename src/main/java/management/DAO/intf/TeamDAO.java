package management.DAO.intf;

import management.entity.Players;
import management.entity.Teams;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/7/2017.
 */

public interface TeamDAO {
    Teams findByTeamId(int id);

    void saveTeam(Teams teams);

    void deleteTeamById(int id);

    List<Teams> findAllTeams();

    List<Players> listPlayers(int id);

    List<Players> abandonPlayers();

    void addIntoTeam(int player_id, int team_id);

    List<Teams> findAllTeamsBySeasonName(String name);
}
