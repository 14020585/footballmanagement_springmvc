package management.DAO.intf;

import management.entity.Players;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
public interface PlayerDAO {
    Players findById(int id);

    void savePlayer(Players players);

    void deletePlayerById(int id);

    void setNullTeamId (Players players);

    List<Players> findAllPlayers();

    List<Players> findAllPlayersByTeamId(int teamId);

//    Players findPlayerByName(String name);
}
