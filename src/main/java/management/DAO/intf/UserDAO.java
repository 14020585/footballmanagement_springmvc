package management.DAO.intf;

import management.entity.GuessScore;
import management.entity.User;

/**
 * Created by Hong Hai Nguyen on 8/8/2017.
 */
public interface UserDAO {
    User findByUsername(String username);


}
