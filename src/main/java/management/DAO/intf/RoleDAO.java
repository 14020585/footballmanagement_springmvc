package management.DAO.intf;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.management.relation.Role;

/**
 * Created by Hong Hai Nguyen on 7/21/2017.
 */
public interface RoleDAO extends JpaRepository<Role, Integer> {
}
