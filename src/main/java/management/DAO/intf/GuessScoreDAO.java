package management.DAO.intf;

import management.entity.GuessScore;

import java.util.List;

public interface GuessScoreDAO {

    GuessScore findGuessScoreById(Long id);

    void saveGuessScore(GuessScore guess);

    void saveRecord(GuessScore guess);

    boolean checkGuess(int match_id);

    List<GuessScore> listAllGuessScoreByUserId(Long id);
}
