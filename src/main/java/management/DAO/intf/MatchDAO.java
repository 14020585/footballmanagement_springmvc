package management.DAO.intf;

import management.entity.Matches;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/14/2017.
 */
public interface MatchDAO {
    Matches findMatchById(int id);

    void saveMatch(Matches matches);

    void deleteMatch(int id);

    List<Matches> findAllMatches();

    List<Matches> findAllMatchComing();

    List<Matches> findAllMatchOccured();
}
