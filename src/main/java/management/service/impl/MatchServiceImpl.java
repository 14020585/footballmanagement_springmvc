package management.service.impl;

import management.DAO.intf.MatchDAO;
import management.entity.Matches;
import management.service.intf.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/14/2017.
 */
@Service("matchService")
@Transactional
public class MatchServiceImpl implements MatchService {
    @Autowired
    private MatchDAO matchDAO;

    @Override
    public Matches findMatchById(int id) {
        return matchDAO.findMatchById(id);
    }

    @Override
    public void saveMatch(Matches matches) {
        matchDAO.saveMatch(matches);
    }

    @Override
    public void deleteMatchById(int id) {
        matchDAO.deleteMatch(id);
    }

    @Override
    public void updateMatch(Matches matches) {
        Matches entity = findMatchById(matches.getId());
        if (entity != null){
            entity.setHometeam(matches.getHometeam());
            entity.setAwayteam(matches.getAwayteam());
            entity.setStart_time(matches.getStart_time());
            entity.setHome_team_score(matches.getHome_team_score());
            entity.setAway_team_score(matches.getAway_team_score());
            entity.setSeasons(matches.getSeasons());
        }
    }

    @Override
    public List<Matches> findAllMatches() {
        return matchDAO.findAllMatches();
    }

    @Override
    public List<Matches> findAllMatchesComing() {
        return matchDAO.findAllMatchComing();
    }

    @Override
    public List<Matches> findAllMatchesOccured() {
        return matchDAO.findAllMatchOccured();
    }
}
