package management.service.impl;

import management.DAO.intf.PlayerDAO;
import management.entity.Players;
import management.service.intf.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
@Service("playerService")
@Transactional
public class PlayerServiceImpl implements PlayerService {
    @Autowired
    private PlayerDAO dao;


    @Override
    public Players findById(int id) {
        return dao.findById(id);
    }


    @Override
    public void savePlayer(Players players) {
        dao.savePlayer(players);
    }

    @Override
    public void updatePlayer(Players players) {
        Players entity = dao.findById(players.getId());
        if (entity != null){
            entity.setName(players.getName());
            entity.setAge(players.getAge());
            entity.setHeight(players.getHeight());
            entity.setWeight(players.getWeight());
            entity.setJoined(players.getJoined());
            entity.setPosition(players.getPosition());
        }
    }

    @Override
    public void deletePlayerById(int id) {
        dao.deletePlayerById(id);
    }

    @Override
    public void setNullTeamId(Integer playerId) {
        dao.setNullTeamId(dao.findById(playerId));
    }


    @Override
    public List<Players> findAllPlayers() {
        return dao.findAllPlayers();
    }

//    @Override
//    public Players findPlayerByName(String name) {
//        return dao.findPlayerByName(name);
//    }
}
