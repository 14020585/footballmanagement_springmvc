package management.service.impl;

import management.DAO.intf.GuessScoreDAO;
import management.DAO.intf.MatchDAO;
import management.entity.GuessScore;
import management.entity.Matches;
import management.entity.User;
import management.service.intf.GuessScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service("guessScoreService")
@Transactional
public class GuessScoreServiceImpl implements GuessScoreService {
    @Autowired
    private GuessScoreDAO guessDAO;

    @Autowired
    private MatchDAO matchDAO;

    @Override
    public GuessScore findGuessScoreById(Long id) {
        return guessDAO.findGuessScoreById(id);
    }

    @Override
    public void saveGuessScore(GuessScore guess) {
        guessDAO.saveGuessScore(guess);
    }

    @Override
    public void saveRecord(Matches match, User user, GuessScore guess) {

        Matches matches = matchDAO.findMatchById(match.getId());

        GuessScore entity = new GuessScore(matches);

        entity.setMatch(match);
        entity.setUser(user);

        entity.setAway_team_guess(guess.getAway_team_guess());
        entity.setHome_team_guess(guess.getHome_team_guess());

        guessDAO.saveGuessScore(entity);

    }

    @Override
    public boolean checkGuess(int match_id) {

        return guessDAO.checkGuess(match_id);
    }

    @Override
    public List<GuessScore> listAllGuessScoreByUserId(Long id) {
        return guessDAO.listAllGuessScoreByUserId(id);
    }
}
