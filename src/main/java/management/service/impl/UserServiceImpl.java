package management.service.impl;

import management.DAO.intf.UserDAO;
import management.entity.User;
import management.service.intf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.soap.SOAPBinding;

/**
 * Created by Hong Hai Nguyen on 8/8/2017.
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO dao;

    @Override
    public User findByUsername(String username) {
        return dao.findByUsername(username);
    }

    @Override
    public User isUser(User user) {
        User usercheck = findByUsername(user.getUsername());
        if (user.getPassword().equals(usercheck.getPassword())){
            User result = new User();
            result.setId(user.getId());
            result.setPassword(user.getPassword());
            result.setUsername(user.getUsername());
            result.setAdmin(user.isAdmin());
            result.setActive(true);
            return result;
        }
        return null;
    }
}
