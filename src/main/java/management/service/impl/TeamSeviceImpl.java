package management.service.impl;

import management.DAO.intf.TeamDAO;
import management.entity.Players;
import management.entity.Teams;
import management.service.intf.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/7/2017.
 */
@Service("teamService")
@Transactional
public class TeamSeviceImpl implements TeamService {
    @Autowired
    private TeamDAO dao;

    @Override
    public Teams findTeamById(int id) {
        return dao.findByTeamId(id);
    }

    @Override
    public void saveTeam(Teams teams) {
        dao.saveTeam(teams);
    }

    @Override
    public void updateTeam(Teams teams) {
        Teams entity = dao.findByTeamId(teams.getId());
        if (entity != null){
            entity.setName(teams.getName());
            entity.setPlayers(teams.getPlayers());
            entity.setSeasons(teams.getSeasons());
        }
    }

    @Override
    public void deleteTeamById(int id) {
        dao.deleteTeamById(id);
    }

    @Override
    public List<Teams> findAllTeams() {
        return dao.findAllTeams();
    }

    @Override
    public List<Players> listPlayers(int id) {
        return dao.listPlayers(id);
    }

    @Override
    public List<Players> abandonPlayers() {
        return dao.abandonPlayers();
    }

    @Override
    public void addIntoTeam(int player_id, int team_id) {
        dao.addIntoTeam(player_id, team_id);
    }
}
