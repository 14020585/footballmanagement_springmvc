package management.service.intf;

import management.entity.Players;
import management.entity.Teams;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/7/2017.
 */
public interface TeamService {
    Teams findTeamById (int id);

    void saveTeam(Teams teams);

    void updateTeam(Teams teams);

    void deleteTeamById(int id);

    List<Teams> findAllTeams();

    List<Players> listPlayers(int i);

    List<Players> abandonPlayers();

    void addIntoTeam(int player_id, int team_id);
}
