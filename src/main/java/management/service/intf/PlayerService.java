package management.service.intf;

import management.entity.Players;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
public interface PlayerService {

     Players findById (int id);

     void savePlayer(Players players);

     void updatePlayer(Players players);

     void deletePlayerById(int id);

     void setNullTeamId (Integer playerId);

     List<Players> findAllPlayers();

//     Players findPlayerByName(String name);
}
