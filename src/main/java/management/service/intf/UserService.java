package management.service.intf;

import management.entity.User;

/**
 * Created by Hong Hai Nguyen on 8/8/2017.
 */
public interface UserService {

    User findByUsername(String username);

    User isUser(User user);
}
