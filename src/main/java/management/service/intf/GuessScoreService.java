package management.service.intf;

import management.entity.GuessScore;
import management.entity.Matches;
import management.entity.User;

import java.util.List;

public interface GuessScoreService {
    GuessScore findGuessScoreById(Long id);

    void saveGuessScore(GuessScore guess);

    void saveRecord (Matches match, User user, GuessScore guess);

    boolean checkGuess(int match_id);

    List<GuessScore> listAllGuessScoreByUserId(Long id);
}
