package management.service.intf;

import management.entity.Matches;

import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/14/2017.
 */
public interface MatchService {

    Matches findMatchById(int id);

    void saveMatch(Matches matches);

    void deleteMatchById(int id);

    void updateMatch(Matches matches);

    List<Matches> findAllMatches();

    List<Matches> findAllMatchesComing();

    List<Matches> findAllMatchesOccured();

}
