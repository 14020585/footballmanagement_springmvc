package management.controller;

import management.entity.Matches;
import management.entity.Teams;
import management.service.intf.MatchService;
import management.service.intf.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hong Hai Nguyen on 7/8/2017.
 */
@Controller
@RequestMapping("/matches")
public class MatchController {
    @Autowired
    MatchService matchService;

    @Autowired
    TeamService teamService;

    /**
     * This method will display all matches
     */
    @GetMapping(value = {"/", "/list", ""})
    public String listAllMatches(ModelMap model){
        List<Matches> matches = matchService.findAllMatches();
        model.addAttribute("title", "List All Matches");
        model.addAttribute("matches", matches);
        return "match/allmatches";
    }

    /**
     * This method will display all matches are coming
     */
    @GetMapping(value = {"/comingup"})
    public String listAllMatchesComing(ModelMap model){
        List<Matches> matches = matchService.findAllMatchesComing();
        model.addAttribute("title", "Matches are coming up");
        model.addAttribute("matches", matches);
        return "match/allmatches";
    }

    /**
     * This method will display all matches occurred
     */
    @GetMapping(value = {"/occurred"})
    public String listAllMatchesOccured(ModelMap model){
        List<Matches> matches = matchService.findAllMatchesOccured();
        model.addAttribute("title", "Matches occurred");
        model.addAttribute("matches", matches);
        return "match/allmatches";
    }

    /**
     * This method will create a match
     */
    @GetMapping(value = "/new")
    public String creatMatch(ModelMap model){
        Map<String, String> teams = new LinkedHashMap<>();
        for (Teams team : teamService.findAllTeams()) {
            teams.put(String.valueOf(team.getId()), team.getName());
        }
        Matches matches = new Matches();
        model.addAttribute("title", "Create a Match");
        model.addAttribute("matches", matches);
        model.addAttribute("teams", teams);
        model.addAttribute("edit", false);
        return "match/creatematch";
    }

    @PostMapping(value = "/new")
    public String saveMatch(@Valid Matches matches, BindingResult result,
                            ModelMap model) {

        if (result.hasErrors()) {
            return "match/creatematch";
        }

        /*
         *Save player
         */

        matchService.saveMatch(matches);

        model.addAttribute("success", "Match was created successfully");
        return "match/successmatch";
    }

    @GetMapping(value = "/edit-{id}-match")
    public String editMatch(@PathVariable int id, ModelMap model){
        Map<String, String> teams = new LinkedHashMap<>();
        for (Teams team : teamService.findAllTeams()) {
            teams.put(String.valueOf(team.getId()), team.getName());
        }
        Matches matches = matchService.findMatchById(id);
        model.addAttribute("title", "Edit Match");
        model.addAttribute("matches", matches);
        model.addAttribute("teams", teams);
        model.addAttribute("edit", true);
        return "match/creatematch";
    }

    @PostMapping(value = "/edit-{id}-match")
    public String updateMatch(@Valid Matches matches, BindingResult result,
                              ModelMap model, @PathVariable int id){
        Map<String, String> teams = new LinkedHashMap<>();
        for (Teams team : teamService.findAllTeams()) {
            teams.put(String.valueOf(team.getId()), team.getName());
        }

        if (result.hasErrors()) {
            return "match/creatematch";
        }

        matchService.updateMatch(matches);

        model.addAttribute("success", "Match updated successfully");
        return "match/successmatch";
    }

    /**
     * This method will delete a record of matches table
     */

    @GetMapping(value = "/delete-{id}-match")
    public String delete(@PathVariable int id){
        matchService.deleteMatchById(id);
        return "redirect:/matches";
    }
}
