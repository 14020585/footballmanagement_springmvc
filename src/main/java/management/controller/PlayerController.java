package management.controller;

import management.entity.Players;
import management.service.intf.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/3/2017.
 */
@Controller
@RequestMapping("/players")
public class PlayerController {
    @Autowired
    PlayerService playerService;

    /**
     * List all existing players
     */
    @RequestMapping(value = {"/", "/list", ""}, method = RequestMethod.GET)
    public String listPlayers (ModelMap model){
        List<Players> players = playerService.findAllPlayers();
        model.addAttribute("title", "All Players");
        model.addAttribute("players", players);

        return "player/allplayers";
    }

    /**
     * Add a new player
     */
    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String newPlayer(ModelMap model){
        Players players = new Players();
        model.addAttribute("title", "Player Registration");
        model.addAttribute("players", players);
        model.addAttribute("edit", false);

        return "player/createplayer";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
    public String savePlayer(@Valid Players players, BindingResult result,
                               ModelMap model) {

        if (result.hasErrors()) {
            return "player/createplayer";
        }

        /*
         *Save player
         */

        playerService.savePlayer(players);

        model.addAttribute("success", "Player " + players.getName() + " registered successfully");
        return "player/success";
    }


    /*
     * This method will provide the medium to update an existing player.
     */
    @RequestMapping(value = { "/edit-{id}-player" }, method = RequestMethod.GET)
    public String editPlayer(@PathVariable int id, ModelMap model) {
        Players players = playerService.findById(id);
        model.addAttribute("title", "Edit Player");
        model.addAttribute("players", players);
        model.addAttribute("edit", true);
        return "player/createplayer";
    }

    /*
     * This method will be called on form submission, handling POST request for
     * updating player in database. It also validates the user input
     */
    @RequestMapping(value = { "/edit-{id}-player" }, method = RequestMethod.POST)
    public String updatePlayer(@Valid Players players, BindingResult result,
                                 ModelMap model, @PathVariable int id) {

        if (result.hasErrors()) {
            return "player/createplayer";
        }

        playerService.updatePlayer(players);

        model.addAttribute("success", "Player " + players.getName()  + " updated successfully");
        return "player/success";
    }


    /*
     * This method will delete a player by it's id.
     */
    @RequestMapping(value = { "/delete-{id}-player" }, method = RequestMethod.GET)
    public String deletePlayer(@PathVariable int id) {
        playerService.deletePlayerById(id);
        return "redirect:/players";
    }

}
