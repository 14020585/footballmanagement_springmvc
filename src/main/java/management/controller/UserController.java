package management.controller;

import management.entity.Players;
import management.entity.User;
import management.service.intf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 8/8/2017.
 */
@Controller
@RequestMapping(value = {"/", " ", ""})
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String getLogIn(ModelMap model){
        User user = new User();
        model.addAttribute("title", "Log in");
        model.addAttribute("user", user);

        return "login/loginOld";
    }

    @RequestMapping(value = { "/login" }, method = RequestMethod.POST)
    public String logIn(@Valid User user, ModelMap model) {
        model.addAttribute("username", user.getUsername());

        if(userService.isUser(user) != null){
            model.addAttribute("success", "Log in successfully");
            return "player/success";
        }
        else return "Can not access";
    }


}
