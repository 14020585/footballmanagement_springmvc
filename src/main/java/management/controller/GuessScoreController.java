package management.controller;

import management.entity.GuessScore;
import management.entity.Matches;
import management.service.intf.GuessScoreService;
import management.service.intf.MatchService;
import management.service.intf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/guessscore")
public class GuessScoreController {
    @Autowired
    GuessScoreService guessScoreService;

    @Autowired
    MatchService matchService;

    @Autowired
    UserService userService;

    /**
     * This method will display all matches are coming up
     */
    @GetMapping(value = {"/", " ", ""})
    public String listAllMatchesComingUp(ModelMap model){
        List<Matches> matches = matchService.findAllMatchesComing();
        model.addAttribute("title", "Matches are coming up");
        model.addAttribute("matches", matches);

        return "guess/allmatchescomingup";
    }

    @GetMapping("/guess-{match_id}-match")
    public String createGuess(@PathVariable int match_id ,  ModelMap model){

        Matches match = matchService.findMatchById(match_id);

        GuessScore guessScore = new GuessScore();

        guessScore.setMatch(match);

        model.addAttribute("title", "Guess Registration");
        model.addAttribute("guessscore", guessScore);
        model.addAttribute("edit", false);
        return "guess/createguess";
    }

    @PostMapping("/guess-{match_id}-match")
    public String saveGuess(@Valid GuessScore guessScore, BindingResult result, ModelMap model, @PathVariable int match_id){

        if (result.hasErrors()) {
            return "guess/createguess";
        }

        /*
         *Save player
         */

        guessScoreService.saveGuessScore(guessScore);

        return "redirect:/guessscore";
    }
}
