package management.controller;

import management.entity.Players;
import management.entity.Teams;
import management.service.intf.PlayerService;
import management.service.intf.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Hong Hai Nguyen on 7/7/2017.
 */
@Controller
@RequestMapping("/teams")
public class TeamController {
    @Autowired
    TeamService teamService;

    @Autowired
    PlayerService playerService;

    /**
     * List all existing teams
     */
    @RequestMapping(value = {"/", "/list", ""}, method = RequestMethod.GET)
    public String listTeams (ModelMap model){
        List<Teams> teams = teamService.findAllTeams();
        model.put("title", "All Teams");
        model.addAttribute("teams", teams);

        return "team/allteams";
    }

    /**
     * Add a new team
     */
    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String newTeam(ModelMap model){
        Teams teams = new Teams();
        model.addAttribute("title", "Player Registration");
        model.addAttribute("teams", teams);
        model.addAttribute("edit", false);

        return "team/createteam";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
    public String saveTeam(@Valid Teams teams, BindingResult result,
                             ModelMap model) {

        if (result.hasErrors()) {
            return "team/createteam";
        }

        /*
         *Save team
         */

        teamService.saveTeam(teams);

        model.addAttribute("success", "Team " + teams.getName() + " registered successfully");
        return "team/successteam";
    }


    /*
     * This method will provide the medium to update an existing team.
     */
    @RequestMapping(value = { "/edit-{id}-team" }, method = RequestMethod.GET)
    public String editEmployee(@PathVariable int id, ModelMap model) {
        Teams teams = teamService.findTeamById(id);
        model.addAttribute("title", "Edit Player");
        model.addAttribute("teams", teams);
        model.addAttribute("edit", true);
        return "team/createteam";
    }

    /*
     * This method will be called on form submission, handling POST request for
     * updating team in database. It also validates the user input
     */
    @RequestMapping(value = { "/edit-{id}-team" }, method = RequestMethod.POST)
    public String updateTeam(@Valid Teams teams, BindingResult result,
                               ModelMap model, @PathVariable int id) {

        if (result.hasErrors()) {
            return "team/createteam";
        }

        teamService.updateTeam(teams);

        model.addAttribute("success", "Team " + teams.getName()  + " updated successfully");
        return "team/successteam";
    }


    /*
     * This method will delete a team by it's id.
     */
    @RequestMapping(value = { "/delete-{id}-team" }, method = RequestMethod.GET)
    public String deleteTeam(@PathVariable int id) {
        teamService.deleteTeamById(id);
        return "redirect:/teams";
    }

    /**
     * This method will display all members of a team
     */
    @GetMapping("/playersofteam-{id}")
    public String listPlayersOfTeam(@PathVariable int id, ModelMap model){
        List<Players> members = teamService.listPlayers(id);
        model.addAttribute("teams", teamService.findTeamById(id));
        model.addAttribute("title", "Members of Team");
        model.addAttribute("players", members);

        return "team/playersofteam";

    }

    /**
     * This method will display all players of a team
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/playersofteam-{id}/new")
    public String listPlayersToAddIntoTeam(@PathVariable int id, ModelMap model){
        Teams teams = teamService.findTeamById(id);
        List<Players> players = teamService.abandonPlayers();
        model.addAttribute("title", "Add a player into team");
        model.addAttribute("teams", teams);
        model.addAttribute("players", players);

        return "team/addplayerintoteam";
    }


    /**
     * This method will change player's team_id if it is null.
     * It means add an abandon player into team
     * @param team_id
     * @param player_id
     * @return
     */
    @GetMapping("/playersofteam-{team_id}/new/add-{player_id}")
    public String addPlayerIntoTeam(@PathVariable int team_id, @PathVariable int player_id){
        teamService.addIntoTeam(player_id, team_id);

        return "redirect:/teams/playersofteam-{team_id}";
    }

    /*
     * This method will provide the medium to update an existing player.
     */
    @RequestMapping(value = { "/playersofteam-{team_id}/edit-{id}-player" }, method = RequestMethod.GET)
    public String editPlayer(@PathVariable int team_id, @PathVariable int id, ModelMap model) {
        Players players = playerService.findById(id);
        model.addAttribute("title", "Edit Player");
        model.addAttribute("players", players);
        model.addAttribute("edit", true);
        return "player/createplayer";
    }

    /*
     * This method will be called on form submission, handling POST request for
     * updating player of a team in database. It also validates the user input
     */
    @RequestMapping(value = { "/playersofteam-{team_id}/edit-{id}-player" }, method = RequestMethod.POST)
    public String updatePlayer(@Valid Players players, BindingResult result,
                               ModelMap model, @PathVariable int team_id, @PathVariable int id) {

        if (result.hasErrors()) {
            return "player/createplayer";
        }

        playerService.updatePlayer(players);

        model.addAttribute("success", "Player " + players.getName()  + " updated successfully");
        return "redirect:/teams/playersofteam-{team_id}";
    }


    /*
     * This method will delete a player of a team by player's id.
     */
    @RequestMapping(value = { "/playersofteam-{team_id}/delete-{id}-player" }, method = RequestMethod.GET)
    public String deletePlayer(@PathVariable int team_id, @PathVariable int id) {
       playerService.setNullTeamId(id);
        return "redirect:/teams/playersofteam-{team_id}";
    }
}