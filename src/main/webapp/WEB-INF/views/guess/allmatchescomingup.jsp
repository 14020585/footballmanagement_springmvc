<%--
  Created by IntelliJ IDEA.
  User: GEM
  Date: 8/18/2017
  Time: 10:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        .table_info tr:first-child{
            font-weight: bold;
            background-color: #27ae60;
        }

        .popuptext tr:first-child{
            font-weight: normal;
            background-color: #555;
        }
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 450px}

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            background-color: #f1f1f1;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }

        body{
            margin:0;
        }

        table{
            border-collapse:collapse;
            width:100%;
            text-align:center;

        }

        .slogan{
            font-size: 30px;
            font-family: Arial;
            text-align: center;
        }

        h2{
            font-size: 32px;
            font-family: Georgia;
        }

        table-header{
            font-weight:bold;
            height:50px;
        }
        .table_info tr{
            height:30px;
        }
        .table_info td{
            padding:10px;
            border:1px solid #ddd;
            border-color: #7f8c8d;

        }
        .content {
            height: 100%;
        }

        .col-sm-2{
            height: 100%;
        }

        /* Popup container - can be anything you want */
        .popup {
            position: relative;
            display: inline-block;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* The actual popup */
        .popup .popuptext {
            visibility: hidden;
            width: 350px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 8px 8px;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -80px;
        }

        /* Popup arrow */
        .popup .popuptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        /* Toggle this class - hide and show the popup */
        .popup .show {
            visibility: visible;
            -webkit-animation: fadeIn 1s;
            animation: fadeIn 1s;
        }

        /* Add animation (fade in the popup) */
        @-webkit-keyframes fadeIn {
            from {opacity: 0;}
            to {opacity: 1;}
        }

        @keyframes fadeIn {
            from {opacity: 0;}
            to {opacity:1 ;}
        }

        .popup td {
            border: none;
        }

    </style>

</head>

<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">GEM Ladies Football</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
                <li><a href="/players">Players</a></li>
                <li><a href="/teams">Football Clubs</a></li>
                <%--<li class="active"><a href="#">Matches</a></li>--%>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Matches
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="/matches">All Matches</a></li>
                        <li><a href="/matches/occurred">Occurred Matches</a></li>
                        <li><a href="/matches/comingup">Coming Up Matches</a></li>
                    </ul>
                </li>
                <li><a href="/guessscore/">Guess Score</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <p><a href="#">Here will be a link of football news in the world</a></p>
            <hr>
            <p><a href="#">Here will be Viet Nam football news link</a></p>
        </div>

        <div class="col-sm-8 text-left">
            <div>
                <h2>LIST MATCHES COMING UP</h2>
                <hr>
            </div>

            <div class="table">
                <table class="table_info">
                    <tr class="table_header">
                        <td>No</td>
                        <%--<td>Date</td>--%>
                        <td>Time</td>
                        <td>Home Team</td>
                        <%--<td>Score</td>--%>
                        <td>Away Team</td>
                        <%--<td>Season</td>--%>
                        <%--<td>Edit</td>--%>
                        <%--<td>Delete</td>--%>
                        <td>Guess Score</td>
                    </tr>
                    <c:forEach items="${matches}" var="match" varStatus="i">
                        <tr>
                            <td>${i.index + 1}</td>
                            <td><fmt:formatDate value="${match.start_time}" pattern="dd/MM/yyyy HH:mm" /></td>
                                <%--<td><fmt:formatDate value="${match.start_time}" pattern="HH:mm" /> </td>--%>
                            <td>${match.hometeam.name}</td>
                            <%--<td>${match.home_team_score} - ${match.away_team_score}</td>--%>
                            <td>${match.awayteam.name}</td>
                                <%--<td>${match.season.name}</td>--%>
                            <td><a href="<c:url value='/guessscore/guess-${match.id}-match' />">Guess</a></td>


                        </tr>
                    </c:forEach>

                </table>
            </div>



        </div>

        <div class="col-sm-2 sidenav">
            <div class="well">
                <p>Here will be something about GEM ladies league</p>
            </div>
            <div class="well">
                <p>Here will be a photo</p>
            </div>
        </div>
    </div>
</div>

<%@include file="../footer.jsp"%>

<script>
    // When the user clicks on div, open the popup
    function myFunction() {
        var popup = document.getElementById("myPopup");
        popup.classList.toggle("show");
    }
</script>

</body>

</html>
