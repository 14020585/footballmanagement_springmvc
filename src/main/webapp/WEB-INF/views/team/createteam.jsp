<%--
  Created by IntelliJ IDEA.
  User: Hong Hai Nguyen
  Date: 7/7/2017
  Time: 5:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>${title}</title>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /*tr:first-child{*/
        /*font-weight: bold;*/
        /*background-color: lightcoral;*/
        /*}*/
        .error {
            color: #4aff6f;
        }
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 450px}

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            background-color: #f1f1f1;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }

        body{
            margin:0;
        }

        /*table{*/
        /*border-collapse:collapse;*/
        /*width:100%;*/
        /*text-align:center;*/

        /*}*/

        .slogan{
            font-size: 30px;
            font-family: Arial;
            text-align: center;
        }

        h2{
            font-size: 32px;
            font-family: Georgia;
        }

        table-header{
            font-weight:bold;
            height:50px;
        }
        tr{
            height:30px;
        }
        /*td{*/
        /*padding:10px;*/
        /*border:1px solid #ddd;*/
        /*border-color: plum;*/

        /*}*/
        .content {
            height: 100%;
        }

        label {
            width: 150px;
            text-align: left;
        }

        /*.formhead{*/
        /*background-color: antiquewhite;*/
        /*}*/

        /*body{*/
        /*background-image: url("");*/
        /*}*/

    </style>

</head>

<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">GEM Ladies Football</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="#">Home</a></li>
                <li><a href="/players">Players</a></li>
                <li class="active"><a href="/teams">Football Clubs</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Matches
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="/matches">All Matches</a></li>
                        <li><a href="/matches/occurred">Occurred Matches</a></li>
                        <li><a href="/matches/comingup">Coming Up Matches</a></li>
                    </ul>
                </li>
                <li><a href="/guessscore/">Guess Score</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <p><a href="#">Here will be a link of football news in the world</a></p>
            <hr>
            <p><a href="#">Here will be Viet Nam football news link</a></p>
        </div>
        <div class="col-sm-8 text-left">
            <div class="formhead">
                <h2>Player Registration</h2>
                <hr>
            </div>

            <div class="table">
                <form:form method="POST" modelAttribute="teams">
                    <%--<form:input type="hidden" path="id" id="id"/>--%>
                    <table>
                        <tr>
                            <td><label for="name">Name: </label> </td>
                            <td><form:input path="name" id="name" class="form-control"/></td>
                            <td><form:errors path="name" cssClass="error"/></td>
                        </tr>

                        <%--<tr>--%>
                            <%--<td><label for="">Name: </label> </td>--%>
                            <%--<td><form:input path="name" id="name"/></td>--%>
                            <%--<td><form:errors path="name" cssClass="error"/></td>--%>
                        <%--</tr>--%>
                        <tr></tr>

                        <tr>
                            <td colspan="3">
                                <c:choose>
                                    <c:when test="${edit}">
                                        <input type="submit" value="Update"/>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="submit" value="Register"/>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>

                    </table>
                </form:form>
            </div>

        </div>

        <div class="col-sm-2 sidenav">
            <div class="well">
                <p>Here will be something about GEM ladies league</p>
            </div>
            <div class="well">
                <p>Here will be a photo</p>
            </div>
        </div>
    </div>
</div>

<%@include file="../footer.jsp"%>

<br/>
<br/>
Go back to <a href="<c:url value='/teams' />">List of all teams</a>
</body>
</html>
